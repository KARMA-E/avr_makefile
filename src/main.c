/*
 * main.c
 *
 *  Created on: 25 окт. 2021 г.
 *      Author: KARMA
 */

#include <avr/io.h>
#include <util/delay.h>
#include "gpio.h"

int main (void)
{
   GPIO_Init();
   while(1)
   {
      GPIO_On();
      _delay_ms (100);
      GPIO_Off();
      _delay_ms (100);
   }
}
