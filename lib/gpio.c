#include <avr/io.h>

#define PIN		PB1

void GPIO_Init(void)
{
	DDRB |= (1 << PIN);
}

void GPIO_On(void)
{
	PORTB |= (1 << PIN);
}

void GPIO_Off(void)
{
	PORTB &= ~(1 << PIN);
}
