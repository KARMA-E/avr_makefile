/*
 * gpio.h
 *
 *  Created on: 26 окт. 2021 г.
 *      Author: KARMA
 */

#ifndef _GPIO_H_
#define _GPIO_H_

void GPIO_Init(void);
void GPIO_On(void);
void GPIO_Off(void);

#endif /* GPIO_H_ */
